package csg.workspace;

import csg.CourseSiteGeneratorApp;
import csg.data.Recitation;
import csg.data.Schedule;
import csg.data.Student;
import csg.data.Team;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

public class CSGWorkspace extends AppWorkspaceComponent {
    
    CourseSiteGeneratorApp app;
    PropertiesManager props;
    CSGController controller;

    // Tab controls
    TabPane tabs;
    Tab detailsTab, taTab, recitationTab, scheduleTab, projectTab;
    
    // Recitation tab controls
    Pane reciTabContent, reciHeaderHBox, reciAddUpdateGridPane;
    Label reciTabHeader, reciAddUpdateHeader, reciSectionLabel, reciInstructorLabel, reciDayTimeLabel, reciLocationLabel, reciFirstTaLabel, reciSecondTaLabel;
    Button reciRemoveButton, reciAddUpdateButton, reciClearButton;
    TableView<Recitation> reciTable;
    TableColumn reciSectionCol, reciInstructorCol, reciDayTimeCol, reciLocationCol, reciFirstTaCol, reciSecondTaCol;
    TextField reciSectionField, reciInstructorField, reciDayTimeField, reciLocationField;
    ComboBox reciFirstTaComboBox, reciSecondTaComboBox;
    
    // Schedule tab controls
    Pane scheduleTabContent, calendarBoundaryBox, scheduleTableHeaderBox;
    Pane scheduleAddUpdateGridPane;
    Label scheduleTabHeader, calendarBoundaryHeader, calendaryStartLabel, calendarEndLabel, scheduleTableHeader;
    Label scheduleAddUpdateHeader, scheduleTypeLabel, scheduleDateLabel, scheduleTimeLabel, scheduleTitleLabel;
    Label scheduleTopicLabel, scheduleLinkLabel, scheduleCriteriaLabel;
    Button scheduleRemoveButton, scheduleAddUpdateButton, scheduleClearButton;
    TableView<Schedule> scheduleTable;
    ComboBox scheduleTypeComboBox;
    TextField scheduleTimeField, scheduleTitleField, scheduleTopicField, scheduleLinkField, scheduleCriteriaField;
    DatePicker calendarStartDatePicker, calendarEndDatePicker, scheduleDatePicker;
    
    
    // Project tab controls
    Pane projectTabContent, teamHeaderBox, studentHeaderBox, teamAddUpdateGridPane, studentAddUpdateGridPane;
    Label projectTabHeader, teamsHeaderLabel, teamAddUpdateHeader, studentsHeaderLabel, studentAddUpdateHeader;
    TableView<Team> teamTable;
    TableColumn teamNameCol, teamColorCol, teamTextColorCol, teamLinkCol;
    Label teamNameLabel, teamColorLabel, teamTextColorLabel, teamLinkLabel;
    ColorPicker teamColorPicker, teamTextColorPicker;
    Button teamRemoveButton, teamAddUpdateButton, teamClearButton;
    TextField teamNameField, teamLinkField;
    TableView<Student> studentTable;
    TableColumn studentFirstNameCol, studentLastNameCol, studentTeamCol, studentRoleCol;
    Label studentFirstNameLabel, studentLastNameLabel, studentTeamLabel, studentRoleLabel;
    ComboBox studentTeamComboBox;
    Button studentRemoveButton, studentAddUpdateButton, studentClearButton;
    TextField studentFirstNameField, studentLastNameField, studentRoleField;
   
    public CSGWorkspace(CourseSiteGeneratorApp initApp) {
        
        app = initApp;props = PropertiesManager.getPropertiesManager();
        
        initTabs();
        
        initDetailsTab();
        initTaTab();
        initRecitationTab();
        initScheduleTab();
        initProjectTab();
        // Put everything in the workspace
        workspace = new BorderPane();
        ((BorderPane) workspace).setCenter(tabs);
        
    }
    
    private void initDetailsTab() {
        
    }
    
    private void initTaTab() {
        
    }
    
    private void initScheduleTab() {
        scheduleTabContent = new VBox();
        
        scheduleTabHeader = new Label("Schedule");
        calendarBoundaryHeader = new Label("Calendar Boundaries");
        
        calendarBoundaryBox = new HBox();
        calendaryStartLabel = new Label("Starting: ");
        calendarStartDatePicker = new DatePicker();
        calendarEndLabel = new Label("Ending: ");
        calendarEndDatePicker = new DatePicker();
        calendarBoundaryBox.getChildren().addAll(   calendaryStartLabel, calendarStartDatePicker,
                                                    calendarEndLabel, calendarEndDatePicker);
        
        scheduleTableHeaderBox = new HBox();
        scheduleTableHeader = new Label("Schedule Items");
        scheduleRemoveButton = new Button("-");
        scheduleTableHeaderBox.getChildren().addAll(scheduleTableHeader, scheduleRemoveButton);
        
        initScheduleTable();
        initScheduleAddUpdateGridPane();
        
        scheduleTabContent.getChildren().addAll(scheduleTabHeader,
                                                calendarBoundaryHeader, calendarBoundaryBox,
                                                scheduleTableHeaderBox, scheduleTable,
                                                scheduleAddUpdateGridPane);
        scheduleTab.setContent(scheduleTabContent);
    }
    private void initScheduleTable() {
        scheduleTable = new TableView<>();
        
        TableColumn scheduleTypeCol = new TableColumn("Type");
        TableColumn scheduleDateCol = new TableColumn("Date");
        TableColumn scheduleTitleCol = new TableColumn("Title");
        TableColumn scheduleTopicCol = new TableColumn("Topic");
        
        scheduleTable.getColumns().addAll(scheduleTypeCol, scheduleDateCol, scheduleTitleCol, scheduleTopicCol);
    }
    private void initScheduleAddUpdateGridPane() {
        scheduleAddUpdateGridPane = new GridPane();
        
        scheduleAddUpdateHeader = new Label("Add/Update");
        scheduleTypeLabel = new Label("Type");
        scheduleTypeComboBox = new ComboBox();
        scheduleDateLabel = new Label("Date");
        scheduleDatePicker = new DatePicker();
        scheduleTimeLabel = new Label("Time");
        scheduleTimeField = new TextField();
        scheduleTitleLabel = new Label("Title");
        scheduleTitleField = new TextField();
        scheduleTopicLabel = new Label("Topic");
        scheduleTopicField = new TextField();
        scheduleLinkLabel = new Label("Link");
        scheduleLinkField = new TextField();
        scheduleCriteriaLabel = new Label("Criteria");
        scheduleCriteriaField = new TextField();
        scheduleAddUpdateButton = new Button("Add");
        scheduleClearButton = new Button("Clear");
        
        GridPane.setConstraints(scheduleAddUpdateHeader, 0, 0);
        GridPane.setConstraints(scheduleTypeLabel, 0, 1);
        GridPane.setConstraints(scheduleTypeComboBox, 1, 1);
        GridPane.setConstraints(scheduleDateLabel, 0, 2);
        GridPane.setConstraints(scheduleDatePicker, 1, 2);
        GridPane.setConstraints(scheduleTimeLabel, 0, 3);
        GridPane.setConstraints(scheduleTimeField, 1, 3);
        GridPane.setConstraints(scheduleTitleLabel, 0, 4);
        GridPane.setConstraints(scheduleTitleField, 1, 4);
        GridPane.setConstraints(scheduleTopicLabel, 0, 5);
        GridPane.setConstraints(scheduleTopicField, 1, 5);
        GridPane.setConstraints(scheduleLinkLabel, 0, 6);
        GridPane.setConstraints(scheduleLinkField, 1, 6);
        GridPane.setConstraints(scheduleCriteriaLabel, 0, 7);
        GridPane.setConstraints(scheduleCriteriaField, 1, 7);
        GridPane.setConstraints(scheduleAddUpdateButton, 0, 8);
        GridPane.setConstraints(scheduleClearButton, 1, 8);
    
        scheduleAddUpdateGridPane.getChildren().addAll( scheduleAddUpdateHeader,
                                                        scheduleTypeLabel, scheduleTypeComboBox,
                                                        scheduleDateLabel, scheduleDatePicker,
                                                        scheduleTimeLabel, scheduleTimeField,
                                                        scheduleTitleLabel, scheduleTitleField,
                                                        scheduleTopicLabel, scheduleTopicField,
                                                        scheduleLinkLabel, scheduleLinkField, 
                                                        scheduleCriteriaLabel, scheduleCriteriaField,
                                                        scheduleAddUpdateButton, scheduleClearButton);
    }
    
    private void initTabs() {
        tabs = new TabPane();
        tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE); 
        
        detailsTab = new Tab();
        taTab = new Tab();
        recitationTab = new Tab();
        scheduleTab = new Tab();
        projectTab = new Tab();
        
        detailsTab.setText("Course Details");
        taTab.setText("TA Data");
        recitationTab.setText("Recitation Data");
        scheduleTab.setText("Schedule Data");
        projectTab.setText("Project Data");
        
        tabs.getTabs().add(detailsTab);
        tabs.getTabs().add(taTab);
        tabs.getTabs().add(recitationTab);
        tabs.getTabs().add(scheduleTab);
        tabs.getTabs().add(projectTab);
    }
    
    private void initRecitationTab() {
        reciTabContent = new VBox();
        
        reciHeaderHBox = new HBox();
        reciTabHeader = new Label("Recitation");
        reciRemoveButton = new Button("-");
        reciHeaderHBox.getChildren().addAll(reciTabHeader, reciRemoveButton);
        
        initReciTable();
        initReciAddUpdateGridPane();
        
        reciTabContent.getChildren().addAll(reciHeaderHBox, reciTable, reciAddUpdateGridPane);
        recitationTab.setContent(reciTabContent);
    }
    private void initReciTable() {
        reciTable = new TableView<>();
        
        reciSectionCol = new TableColumn("Section");
        reciInstructorCol = new TableColumn("Instructor");
        reciDayTimeCol = new TableColumn("DayTime");
        reciLocationCol = new TableColumn("Location");
        reciFirstTaCol = new TableColumn("TA");
        reciSecondTaCol = new TableColumn("TA");
        
        reciTable.getColumns().addAll(  reciSectionCol, reciInstructorCol, reciDayTimeCol,
                                        reciLocationCol, reciFirstTaCol, reciSecondTaCol);
    }
    private void initReciAddUpdateGridPane() {
        reciAddUpdateGridPane = new GridPane();
        reciAddUpdateGridPane.setPadding(new Insets(10, 10, 10, 10));
        
        reciAddUpdateHeader = new Label("Add/Update");
        reciSectionLabel = new Label("Section");
        reciSectionField = new TextField();
        reciInstructorLabel = new Label("Instructor");
        reciInstructorField = new TextField();
        reciDayTimeLabel = new Label("DayTime");
        reciDayTimeField = new TextField();
        reciLocationLabel = new Label("Location");
        reciLocationField = new TextField();
        reciFirstTaLabel = new Label("Supervising TA");
        reciFirstTaComboBox = new ComboBox();
        reciSecondTaLabel = new Label("Supervising TA");
        reciSecondTaComboBox = new ComboBox();
        reciAddUpdateButton = new Button("Add");
        reciClearButton = new Button("Clear");
        
        GridPane.setConstraints(reciAddUpdateHeader, 0, 0);
        GridPane.setConstraints(reciSectionLabel, 0, 1);
        GridPane.setConstraints(reciSectionField, 1, 1);
        GridPane.setConstraints(reciInstructorLabel, 0, 2);
        GridPane.setConstraints(reciInstructorField, 1, 2);
        GridPane.setConstraints(reciDayTimeLabel, 0, 3);
        GridPane.setConstraints(reciDayTimeField, 1, 3);
        GridPane.setConstraints(reciLocationLabel, 0, 4);
        GridPane.setConstraints(reciLocationField, 1, 4);
        GridPane.setConstraints(reciFirstTaLabel, 0, 5);
        GridPane.setConstraints(reciFirstTaComboBox, 1, 5);
        GridPane.setConstraints(reciSecondTaLabel, 0, 6);
        GridPane.setConstraints(reciSecondTaComboBox, 1, 6);
        GridPane.setConstraints(reciAddUpdateButton, 0, 7);
        GridPane.setConstraints(reciClearButton, 1, 7);
        
        reciAddUpdateGridPane.getChildren().addAll( reciAddUpdateHeader,
                                                    reciSectionLabel, reciSectionField,
                                                    reciInstructorLabel, reciInstructorField,
                                                    reciDayTimeLabel, reciDayTimeField,
                                                    reciLocationLabel, reciLocationField,
                                                    reciFirstTaLabel, reciFirstTaComboBox,
                                                    reciSecondTaLabel, reciSecondTaComboBox,
                                                    reciAddUpdateButton, reciClearButton);
    }
    
    private void initProjectTab() {
        projectTabContent = new VBox();
        
        teamHeaderBox = new HBox();
        teamsHeaderLabel = new Label("Teams");
        teamRemoveButton = new Button("-");
        teamHeaderBox.getChildren().addAll(teamsHeaderLabel, teamRemoveButton);

        initTeamTable();
        initTeamAddUpdateGridPane();
        
        studentHeaderBox = new HBox();
        studentsHeaderLabel = new Label("Students");
        studentRemoveButton = new Button("-");
        studentHeaderBox.getChildren().addAll(studentsHeaderLabel, studentRemoveButton);
        
        initStudentTable();
        initStudentAddUpdateGridPane();
        
        projectTabContent.getChildren().addAll( teamHeaderBox,
                                                teamTable,
                                                teamAddUpdateGridPane,
                                                studentHeaderBox,
                                                studentTable,
                                                studentAddUpdateGridPane);
        projectTab.setContent(projectTabContent);
    }
    private void initTeamTable() {
        teamTable = new TableView<>();
        
        teamNameCol = new TableColumn("Name");
        teamColorCol = new TableColumn("Color");
        teamTextColorCol = new TableColumn("Text Color");
        teamLinkCol = new TableColumn("Link");
        
        teamTable.getColumns().addAll(  teamNameCol, teamColorCol,
                                        teamTextColorCol, teamLinkCol);
    }
    private void initTeamAddUpdateGridPane() {
        teamAddUpdateGridPane = new GridPane();
        teamAddUpdateGridPane.setPadding(new Insets(10, 10, 10, 10));
        
        teamAddUpdateHeader =  new Label("Add/Update");
        teamNameLabel = new Label("Name");
        teamNameField = new TextField();
        teamColorLabel = new Label("Color");
        teamColorPicker = new ColorPicker();
        teamTextColorLabel = new Label("Text Color");
        teamTextColorPicker = new ColorPicker();
        teamLinkLabel = new Label("Link");
        teamLinkField = new TextField();
        teamAddUpdateButton = new Button("Add");
        teamClearButton = new Button("Clear");
        
        GridPane.setConstraints(teamAddUpdateHeader, 0, 0);
        GridPane.setConstraints(teamNameLabel, 0, 1);
        GridPane.setConstraints(teamNameField, 1, 1);
        GridPane.setConstraints(teamColorLabel, 0, 2);
        GridPane.setConstraints(teamColorPicker, 1, 2);
        GridPane.setConstraints(teamTextColorLabel, 0, 3);
        GridPane.setConstraints(teamTextColorPicker, 1, 3);
        GridPane.setConstraints(teamLinkLabel, 0, 4);
        GridPane.setConstraints(teamLinkField, 1, 4);
        GridPane.setConstraints(teamAddUpdateButton, 0, 5);
        GridPane.setConstraints(teamClearButton, 1, 5);
        
        teamAddUpdateGridPane.getChildren().addAll( teamAddUpdateHeader,
                                                    teamNameLabel, teamNameField,
                                                    teamColorLabel, teamColorPicker,
                                                    teamTextColorLabel, teamTextColorPicker,
                                                    teamLinkLabel, teamLinkField,
                                                    teamAddUpdateButton, teamClearButton);
    }
    private void initStudentTable() {
        studentTable = new TableView<>();
        
        studentFirstNameCol = new TableColumn("First Name");
        studentLastNameCol = new TableColumn("Last Name");
        studentTeamCol = new TableColumn("Team");
        studentRoleCol = new TableColumn("Role");
        
        studentTable.getColumns().addAll(   studentFirstNameCol, studentLastNameCol,
                                            studentTeamCol, studentRoleCol);
    }
    private void initStudentAddUpdateGridPane() {
        studentAddUpdateGridPane = new GridPane();
        studentAddUpdateGridPane.setPadding(new Insets(10, 10, 10, 10));
        
        studentAddUpdateHeader = new Label("Add/Update");
        studentFirstNameLabel = new Label("First Name");
        studentFirstNameField = new TextField();
        studentLastNameLabel = new Label("Last Name");
        studentLastNameField = new TextField();
        studentTeamLabel = new Label("Team");
        studentTeamComboBox = new ComboBox();
        studentRoleLabel = new Label("Role");
        studentRoleField = new TextField();
        studentAddUpdateButton = new Button("Add");
        studentClearButton = new Button("Clear");
        
        GridPane.setConstraints(studentAddUpdateHeader, 0, 0);
        GridPane.setConstraints(studentFirstNameLabel, 0, 1);
        GridPane.setConstraints(studentFirstNameField, 1, 1);
        GridPane.setConstraints(studentLastNameLabel, 0, 2);
        GridPane.setConstraints(studentLastNameField, 1, 2);
        GridPane.setConstraints(studentTeamLabel, 0, 3);
        GridPane.setConstraints(studentTeamComboBox, 1, 3);
        GridPane.setConstraints(studentRoleLabel, 0, 4);
        GridPane.setConstraints(studentRoleField, 1, 4);
        GridPane.setConstraints(studentAddUpdateButton, 0, 5);
        GridPane.setConstraints(studentClearButton, 1, 5);
        
        studentAddUpdateGridPane.getChildren().addAll(  studentAddUpdateHeader,
                                                        studentFirstNameLabel, studentFirstNameField,
                                                        studentLastNameLabel, studentLastNameField,
                                                        studentTeamLabel, studentTeamComboBox,
                                                        studentRoleLabel, studentRoleField,
                                                        studentAddUpdateButton, studentClearButton);
    }
    
    @Override
    public void resetWorkspace() { 
    
    }
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) { 
    
    }
    
    // Accessor methods for all controls needed by the controller to handle user actions
    
    // Accessor methods for recitation tab controls
    public TableView getReciTable() { return reciTable; }
    public TextField getReciSectionField() { return reciSectionField; }
    public TextField getReciInstructorField() { return reciInstructorField; }
    public TextField getReciDayTimeField() { return reciDayTimeField; }
    public TextField getReciLocationField() { return reciLocationField; }
    public ComboBox getReciFirstTaComboBox() { return reciFirstTaComboBox; }
    public ComboBox getReciSecondTaComboBox() { return reciSecondTaComboBox; }
    public Button getReciAddUpdateButton() { return reciAddUpdateButton; }
    
    // Accessor methods for project tab controls
    public TableView getTeamTable() { return teamTable; }
    public TextField getTeamNameField() { return teamNameField; }
    public ColorPicker getTeamColorPicker() { return teamColorPicker; }
    public ColorPicker getTeamTextColorPicker() { return teamTextColorPicker; }
    public TextField getTeamLinkField() { return teamLinkField; }
    public Button getTeamAddUpdateButton() { return teamAddUpdateButton; }
    public TableView getStudentTable() { return studentTable; }
    public TextField getStudentFirstNameField() { return studentFirstNameField; }
    public TextField getStudentLastNameField() { return studentLastNameField; }
    public ComboBox getStudentTeamComboBox() { return studentTeamComboBox; }
    public TextField getStudentRoleField() { return studentRoleField; }
    public Button getStudentAddUpdateButton() { return studentAddUpdateButton; }
    
}
