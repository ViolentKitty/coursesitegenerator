package csg.workspace;

import csg.CourseSiteGeneratorApp;
import djf.ui.AppGUI;

public class CSGController {

    CourseSiteGeneratorApp app;

    public CSGController(CourseSiteGeneratorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }

}

    